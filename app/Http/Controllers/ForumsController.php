<?php

namespace App\Http\Controllers;
use App\Forum;
use Illuminate\Http\Request;

class ForumsController extends Controller
{
    public function index ()
    {
        $Forums = Forum::latest()->paginate(10);
        //dd($Forums);
        return view('forums.index',compact('Forum'));
    }
}
